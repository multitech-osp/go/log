package log

import (
	"os"

	"github.com/sirupsen/logrus"
)

var log *logrus.Logger

func Init() error {
	log = logrus.New()

	log.SetFormatter(&logrus.JSONFormatter{})

	// Output to stdout instead of the default stderr
	log.SetOutput(os.Stdout)

	// set level for logrus
	logLevel, err := logrus.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		logLevel = logrus.InfoLevel
	}
	log.SetLevel(logLevel)

	return nil
}

func Info(msg string, content ...interface{}) {
	logWithFields(content...).Info(msg)
}

func Debug(msg string, content ...interface{}) {
	logWithFields(content...).Debug(msg)
}

func Warning(msg string, content ...interface{}) {
	logWithFields(content...).Warning(msg)
}

func Error(msg string, content ...interface{}) {
	logWithFields(content...).Error(msg)
}

func Panic(msg string, content ...interface{}) {
	logWithFields(content...).Panic(msg)
}

func Fatal(msg string, content ...interface{}) {
	logWithFields(content...).Fatal(msg)
}

func logWithFields(content ...interface{}) *logrus.Entry {

	hostname, _ := os.Hostname()
	return log.WithFields(logrus.Fields{
		"appname":  os.Getenv("APP_NAME"),
		"hostname": hostname,
		"content":  content,
	})

}
