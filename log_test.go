package log

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var buf bytes.Buffer

func TestMain(m *testing.M) {
	os.Setenv("LOG_LEVEL", "debug")
	Init()
	oldOut := log.Out
	buf = bytes.Buffer{}
	log.SetOutput(&buf)
	defer log.SetOutput(oldOut)
	m.Run()
}

func TestInfo(t *testing.T) {
	Info("Info test")
	assert.Contains(t, buf.String(), "Info test")
}

func TestError(t *testing.T) {
	Error("Error test")
	assert.Contains(t, buf.String(), "Error test")
}

func TestWarning(t *testing.T) {
	Warning("Warning test")
	assert.Contains(t, buf.String(), "Warning test")
}

func TestDebug(t *testing.T) {
	Debug("Debug test")
	assert.Contains(t, buf.String(), "Debug test")
}
